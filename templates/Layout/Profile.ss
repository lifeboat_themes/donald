<main class="main account">
    <div class="page-content mt-10 mb-10">
        <div class="container pt-1">
            <div class="tab tab-vertical">
                <ul class="nav nav-tabs mb-4" role="tablist">
                    <% if $Loyalty.Enable %>
                        <li class="mb-3">
                            <div class="loyalty-points">
                                <small>You currently have</small><br/>
                                <strong>$Customer.LoyaltyPoints</strong> $Loyalty.Name
                            </div>
                        </li>
                    <% end_if %>
                    <li class="nav-item">
                        <a class="nav-link" href="#account">Account details</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#address">Addresses</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#history">Order History</a>
                    </li>
                    <li class="nav-item">
                        <a href="/customer/logout">Logout</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="account">
                        $ProfileDetailsForm
                    </div>
                    <div class="tab-pane" id="address">
                        $ProfileAddressesForm
                    </div>
                    <div class="tab-pane" id="history">
                        $OrderHistory
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>