<header class="header">
        <div class="header-middle sticky-header fix-top sticky-content has-center">
            <div class="container">
                <div class="header-left">
                    <a href="#" class="mobile-menu-toggle">
                        <i class="d-icon-bars2"></i>
                    </a>
                    <a href="/" class="logo d-none d-lg-block">
                        <img src="$SiteSettings.Logo.Fit(163,39).AbsoluteLink" alt="$SiteSettings.Title logo"
                             width="$SiteSettings.Logo.Fit(163,39).getWidth" height="$SiteSettings.Logo.Fit(163,39).getHeight" />
                    </a>
                </div>
                <div class="header-center">
                    <a href="/" class="logo d-lg-none">
                        <img src="$SiteSettings.Logo.Fit(163,39).AbsoluteLink" alt="$SiteSettings.Title logo"
                             width="$SiteSettings.Logo.Fit(163,39).getWidth" height="$SiteSettings.Logo.Fit(163,39).getHeight" />
                    </a>
                    <div class="header-search hs-expanded">
                        <form action="/search" method="get" class="input-wrapper">
                            <input type="search" class="form-control" name="search" id="search"
                                   placeholder="Search for a product...">
                            <button class="btn btn-sm btn-search" type="submit"><i
                                    class="d-icon-search"></i></button>
                        </form>
                    </div>
                    <% include MainMenu %>
                </div>
                <div class="header-right">
                    <% if $Customer %>
                        <a href="$Customers.AbsoluteLink('profile')" class="login">
                            <i class="d-icon-user"></i>
                            <span>Profile</span>
                        </a>
                    <% else %>
                        <a href="$Customers.AbsoluteLink('login')" class="login">
                            <i class="d-icon-user"></i>
                            <span>Login</span>
                        </a>
                    <% end_if %>
                    <span class="divider"></span>
                    <a href="$WishList.AbsoluteLink" class="login">
                        <i class="d-icon-heart-full"></i>
                    </a>
                    <div class="dropdown cart-dropdown">
                        <a href="#" class="cart-toggle">
                            <span class="cart-label">
                                <span class="cart-name">Cart</span>
                                <span class="cart-price">$Cart.FormatPrice('Subtotal', 1)</span>
                            </span>
                            <i class="minicart-icon">
                                <span class="cart-count">$Cart.getItemCount(0)</span>
                            </i>
                        </a>

                        <div class="dropdown-box">
                            <div class="product product-cart-header">
                                <span class="product-cart-counts">$Cart.getItemCount(0) items</span>
                                <span><a href="$Cart.AbsoluteLink">View cart</a></span>
                            </div>
                            <div class="products scrollable">
                                <% loop $Cart.getItems('0') %>
                                <div class="product product-cart" data-role="product"
                                        data-id="$Product.ID" data-variant="$Variant.ID">
                                    <div class="product-detail">
                                        <a href="$Product.Link" class="product-name d-block">
                                            $Product.Title
                                            <small class="text-muted d-block">$Variant</small>
                                        </a>
                                        <div class="price-box">
                                            <span class="product-quantity">$Quantity</span>
                                            <span class="product-price">$Product.FormatPrice('SellingPrice', $Variant.ID)</span>
                                        </div>
                                    </div>
                                    <figure class="product-media">
                                        <a href="$Product.Link">
                                            <% include ProductImage Product=$Product, W=150, H=150 %>
                                        </a>
                                        <button class="btn btn-link btn-close" data-role="delete">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </figure>
                                </div>
                                <% end_loop %>
                            </div>
                            <div class="cart-total">
                                <label>Subtotal:</label>
                                <span class="price">$Cart.FormatPrice('Subtotal', 1)</span>
                            </div>
                            <div class="cart-action">
                                <a href="$Cart.CheckoutLink" class="btn btn-dark"><span>Checkout</span></a>
                            </div>
                        </div>
                    </div>

                    <div class="header-search hs-toggle mobile-search">
                        <a href="#" class="search-toggle">
                            <i class="d-icon-search"></i>
                        </a>
                        <form action="/search" method="get" class="input-wrapper">
                            <input type="search" class="form-control" name="search" autocomplete="off"
                                   placeholder="Search your keyword..." required />
                            <button class="btn btn-search" type="submit">
                                <i class="d-icon-search"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="header-bottom d-lg-show">
            <div class="container">
                <div class="inner-wrap">
                    <div class="header-left">
                        <% include MainMenu %>
                    </div>
                    <div class="header-right">
                        <nav class="custom-menu">
                            <ul class="menu">
                                <li>
                                    <a href="/collections">
                                        <i class="d-icon-bag mr-2"></i>
                                        Shop
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>