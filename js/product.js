const ProductForm = function (form, container) {
    "use strict";

    let _this = this;

    this.make_select_field = function (option, opts) {
        let elem = $("<select>", {'class': "form-control", 'id': option, 'name': option});

        opts.forEach(function (item) {
            elem.append($("<option>", {'value': item}).html(item));
        });

        return elem;
    };

    this.make_select = function (option, name, options) {
        let select_options = [];

        options.forEach(function (item) {
            select_options.push($("<option>", {'value': item}).html(item));
        });

        return $("<div>", {'class': "product-form product-variations"}).html([
            $("<label>", {'for': option}).html(name + ":"),
            $("<div>", {'class': 'ml-1 select-box'}).html(
                _this.make_select_field(option, options)
            )
        ])
    };

    this.render_price = function () {
        let c = container.find(".price-container").html('<i class="fa fa-spinner fa-spin fa-2x"></i>');
        let btn = form.find('.add-to-cart').addClass('disabled').prop('disabled', true);
        let elem = $("<div>", {'class': 'product-price'});

        Lifeboat.Product.CalculatePrice(form.data("id"), _this.get_variant_data(), 1, function (data) {
            if (data.StockAmount === 0) {
                elem.html("<small class='old-price'>Out of Stock</small>");
            } else {
                if (data.isDiscounted) {
                    elem.html([
                        "<ins class='new-price'>" + data.SellingPrice + "</ins>",
                        "<del class='old-price'>" + data.BasePrice + "</del>"
                    ]);
                } else {
                    elem.html(data.SellingPrice);
                }
                btn.removeClass('disabled').prop('disabled', false);
            }

            c.html(elem);
        });
    };

    this.render_variants = function () {
        let v = $("<div>", {'class': "variant-container"});

        v.html('<i class="fa fa-spinner fa-spin fa-2x"></i>');
        Lifeboat.Product.Variants(form.data("id"), function (data) {
            v.html("");

            if (typeof data === 'object' && data !== null) {
                for (let x = 1; x < 4; x++) {
                    let opt_name = 'Option' + x;
                    if (data[opt_name]) {
                        v.append(_this.make_select('Option' + x, data[opt_name].Name, Object.keys(data[opt_name].Options)));
                    }

                    v.find("select#" + opt_name).change(function () {
                        let $this = $(this),
                            elem_id = $this.attr('id'),

                            val = $this.val();

                        for (let y = 1; y < 4; y++) {
                            let check_opt = 'Option' + y;
                            if (elem_id === check_opt) continue;

                            let select  = v.find("select#" + check_opt);
                            let allow   = false;
                            let curr    = select.val();

                            data[elem_id].Options[val][check_opt].forEach(function (item) {
                                if (item === curr) allow = true;
                            });

                            if (!allow) {
                                select.val(data[elem_id].Options[val][check_opt][0]);
                            }
                        }
                        _this.render_price();
                    });
                }

                $(v.find("select")[0]).trigger("change");
            }

            _this.render_price();
        });

        return v;
    };

    this.get_variant_data = function () {
        return {
            "Option1": form.find(".form-control#Option1").val(),
            "Option2": form.find(".form-control#Option2").val(),
            "Option3": form.find(".form-control#Option3").val(),
        };
    };

    this.init = function () {
        let desc = container.find('.product-short-desc');

        container.append($("<div>", {class: "price"}).append([
            $("<div>", {class: "price-container"}),
            desc,
            _this.render_variants(),
        ]));

        form.find(".add-to-cart").click(function (e) {
            e.preventDefault();
            Lifeboat.Cart.AddItem(
                form.data("id"),
                _this.get_variant_data(),
                form.find("#Quantity").val(),
                function () {
                    location.reload();
                }
            );
        });
    }
};

(function($) {
    "use strict";

    let ip = '0.0.0.0';

    $.get('https://www.cloudflare.com/cdn-cgi/trace', function(data) {
        data.split("\n").forEach(function (line) {
            let content = line.split('=');
            if (content[0].toLowerCase() === 'ip') {
                ip = content[1];
                return true;
            }
        });
    });

    $("*[data-role='product']").each(function () {
        let elem = $(this);

        elem.find("*[data-role='quantity']").change(function (e) {
            Lifeboat.Cart.UpdateQuantity(elem.data('id'), elem.data('variant'), $(this).val(), function () { location.reload(); });
        });

        elem.find("*[data-role='delete']").click(function (e) {
            e.preventDefault();
            Lifeboat.Cart.RemoveItem(elem.data('id'), elem.data('variant'), function () { location.reload(); });
        });
    });

    $("*[data-role='product-form']").each(function () {
        let pf = new ProductForm($(this), $($(this).data('form')));
        pf.init();
    });

    $(".wishlist-toggle").click(function (e) {
        e.preventDefault();
        Lifeboat.WishList.Toggle($(this).data("id"), function () {
            location.reload();
        })
    });

    $("*[data-role='add-to-cart']").click(function (e) {
        e.preventDefault();
        Lifeboat.Cart.AddItem($(this).data('id'), '', 1, function () { location.reload(); });
    });

    $('.toolbox-sort select').change(function (e) {
        e.preventDefault();
        window.location = $(this).val();
    });

    $(".mailchimp-subscribe").submit(function (e) {
        e && e.preventDefault();

        let $this = $(this);

        let alert = $($this.data('alert'));
        if (alert.length < 1) alert = $('<div>', {class: 'alert mb-md-4'}).hide().insertBefore($this);

        alert.removeClass('alert-danger alert-success').hide();

        $.post('/mailchimp/subscribe', {
            'email': $this.find('#email').val(),
            'status': 'subscribed',
            'ip_signup': ip,
            'ip_opt': ip
        }, function () {
            alert
                .addClass('alert-success')
                .html('Thank you for subscribing.')
                .show();
            $this.find('input').prop('disabled', true);
        }).fail(function () {
            alert
                .addClass('alert-danger')
                .html('We could not subscribe you right now. Please try again later.')
                .show();
        });
    });
})(jQuery);
